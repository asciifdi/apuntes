# Bienvenido 
a nuestro humilde repositorio de apuntes

## Como descargar apuntes
Navega por las carpetas hasta encontrar lo que buscas

## Como subir apuntes
Puedes:
- Subir el archivo directamente si es de licencia libre mediante
un Pull Request
- Modificar el README.md de la carpeta correspondiente añadiendo un enlace a la lista
- Abrir un [issue](https://gitlab.com/asciifdi/apuntes/-/issues) con lo que hayas encontrado y otra persona se encargará de subirlo
